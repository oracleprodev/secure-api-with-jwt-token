package com.software.secure.demo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/demo")
public class DemoController {

    @GetMapping
    @RequestMapping("/home")
    public ResponseEntity<String> index() {
        return ResponseEntity
                .ok("Hello and Welcome to My Secured Project");
    }
}
