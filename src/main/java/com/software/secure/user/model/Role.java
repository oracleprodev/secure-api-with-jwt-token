package com.software.secure.user.model;

public enum Role {
    ADMIN,
    USER
}
