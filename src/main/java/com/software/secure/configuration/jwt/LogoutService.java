package com.software.secure.configuration.jwt;

import com.software.secure.token.model.Token;
import com.software.secure.token.repository.TokenRepository;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LogoutService implements LogoutHandler {

    private final TokenRepository tokenRepository;

    @Override
    public void logout(HttpServletRequest request,
                       HttpServletResponse response,
                       Authentication authentication) {

        String authHeader = request.getHeader("Authorization");
        String token;

        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            return;
        }

        token = authHeader.substring(7);

        Token fetchToken = tokenRepository.findByToken(token)
                .orElse(null);

        if (fetchToken != null) {
            fetchToken.setExpired(true);
            fetchToken.setRevoked(true);
            tokenRepository.save(fetchToken);
        }
    }
}
