package com.software.secure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecureApiWithJwtTokenApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecureApiWithJwtTokenApplication.class, args);
    }

}
