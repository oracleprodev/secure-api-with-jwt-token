package com.software.secure.auth.controller;

import com.software.secure.auth.dto.request.authentication.AuthenticationRequest;
import com.software.secure.auth.dto.request.register.RegisterRequest;
import com.software.secure.auth.dto.response.AuthenticationResponse;
import com.software.secure.auth.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping
    @RequestMapping("/register")
    public ResponseEntity<AuthenticationResponse> register(
            @RequestBody RegisterRequest registerRequest){

        return ResponseEntity.ok()
                .body(authenticationService.resister(registerRequest));
    }

    @PostMapping
    @RequestMapping("/authenticate")
    public ResponseEntity<AuthenticationResponse> authenticate(
            @RequestBody AuthenticationRequest authenticationRequest){

        return ResponseEntity.ok()
                .body(authenticationService.authenticate(authenticationRequest));
    }
}
