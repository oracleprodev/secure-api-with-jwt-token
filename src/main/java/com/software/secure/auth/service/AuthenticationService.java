package com.software.secure.auth.service;

import com.software.secure.auth.dto.request.authentication.AuthenticationRequest;
import com.software.secure.auth.dto.request.register.RegisterRequest;
import com.software.secure.auth.dto.response.AuthenticationResponse;
import com.software.secure.configuration.jwt.JWTService;
import com.software.secure.token.model.Token;
import com.software.secure.token.model.TokenType;
import com.software.secure.token.repository.TokenRepository;
import com.software.secure.user.model.Role;
import com.software.secure.user.model.User;
import com.software.secure.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final JWTService jwtService;

    private final AuthenticationManager authenticationManager;

    private final TokenRepository tokenRepository;

    public AuthenticationResponse resister(RegisterRequest registerRequest) {

        User user = User.builder()
                .firstname(registerRequest.getFirstname())
                .lastname(registerRequest.getLastname())
                .email(registerRequest.getEmail())
                .password(passwordEncoder.encode(registerRequest.getPassword()))
                .role(Role.USER)
                .build();

        User savedUser = userRepository.save(user);
        String jwt = jwtService.generateToken(savedUser);

        saveUserToken(jwt, savedUser);

        return AuthenticationResponse.builder()
                .token(jwt)
                .build();
    }

    public AuthenticationResponse authenticate(AuthenticationRequest authenticationRequest) {

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getEmail(),
                        authenticationRequest.getPassword()
                )
        );

        User user = userRepository
                .findByEmail(authenticationRequest.getEmail())
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));

        revokeAllTokens(user);

        String jwt = jwtService.generateToken(user);

        saveUserToken(jwt, user);

        return AuthenticationResponse
                .builder()
                .token(jwt)
                .build();
    }

    private void revokeAllTokens(User user) {
        List<Token> allTokensByUserId = tokenRepository.findAllTokensByUserId(user.getId());
        if (allTokensByUserId.isEmpty()) {
            return;
        }
        allTokensByUserId.forEach(token -> {
            token.setExpired(true);
            token.setRevoked(true);
        });

        tokenRepository.saveAll(allTokensByUserId);
    }

    private void saveUserToken(String jwt, User user) {
        Token token = Token.
                builder()
                .token(jwt)
                .tokenType(TokenType.BEARER)
                .user(user)
                .expired(false)
                .revoked(false)
                .build();

        tokenRepository.save(token);
    }
}
